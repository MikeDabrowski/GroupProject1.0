package piotrek;

/**
 * Created by Piotr Góralewski on 2015-12-20.
 */
public class Sciezka{
    private String pktPoczatkowy;
    private String pktKoncowy;
    //private int dlugosc;
    //private int predkosc;
    private double czas;

    public Sciezka(String pktP, String pktK, int dl, int pr)
    {
        pktPoczatkowy = pktP;
        pktKoncowy = pktK;
        czas = (double)dl / (double)pr;
    }

    public Sciezka(String pktP, String pktK, double cz)
    {
        pktPoczatkowy = pktP;
        pktKoncowy = pktK;
        czas = cz;
    }

    public String toString()
    {
        if(pktPoczatkowy.equals(pktKoncowy))
            return "Postoj w punkcie " + pktPoczatkowy + "   (czas postoju = " + czas + ")";
        else
            return pktPoczatkowy + " -> " + pktKoncowy + "               (czas podrozy = " + czas + ")";
        //return pktPoczatkowy + " -> " + pktKoncowy + " w czasie: " + czas;
    }

    public String getPktKoncowy() {
        return pktKoncowy;
    }

    public String getPktPoczatkowy() {
        return pktPoczatkowy;
    }

    public double getCzas() {
        return czas;
    }
}
