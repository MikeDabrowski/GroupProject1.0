package piotrek; /**
 * Created by Piotr Góralewski on 2015-12-27.
 */

import java.io.*;


public class Czytnik
{
    public MojaLista odczytajSciezki(File file) throws IOException
    {
        FileReader plik = new FileReader(file);
        BufferedReader in = new BufferedReader(plik);
        //MojaLista odczytaneSciezki = new MojaLista();
        MojaLista odczytaneSciezki = new MojaLista();

        String linia;
        while( (linia = in.readLine() ) != null )
        {
            String[] dane = linia.split(" ");
            Sciezka temp = new Sciezka(dane[0],dane[1], Integer.parseInt(dane[2]),Integer.parseInt(dane[3]));
            odczytaneSciezki.add(temp);
        }

        return odczytaneSciezki;
    }

}
