package piotrek;


import java.util.ArrayList;

/**
 * Created by Piotr G�ralewski on 2015-12-20.
 */
public class Trasa {

    public Trasa() {
        this.trasa = new MojaLista();
        this.tymczasowaTrasa = new MojaLista();
    }

    private MojaLista trasa;
    private MojaLista tymczasowaTrasa;
    private double czasCalejTrasy = 0;
    private boolean czyMamJuzPorownywacCzas = false;
    private String takiBylPktStartowy; //potrzebne do usuniecia postoju w pkt startowym

    public void znajdzNajszybszaTrase(MojaLista sciezki, String start, String koniec, ArrayList<Double> tymczasowyCzas) //ze sciezek znajduje najszybsza trase pomiedzy start a koniec
    {
        for(int i=0;i<sciezki.size();i++) // sprawdzam czy jest postoj
        {
            if (start.equals(sciezki.get(i).getPktPoczatkowy()) && start.equals(sciezki.get(i).getPktKoncowy()))
            {
                if (start.equals(takiBylPktStartowy))
                {
                    sciezki.remove(i);
                    znajdzNajszybszaTrase(sciezki, start, koniec, tymczasowyCzas);
                    return;
                }
                tymczasowyCzas.add(sciezki.get(i).getCzas());
                tymczasowaTrasa.add(sciezki.get(i));
                Sciezka pom = sciezki.get(i);
                sciezki.remove(i);
                znajdzNajszybszaTrase(sciezki, start, koniec, tymczasowyCzas);
                sciezki.add(pom);
                tymczasowaTrasa.remove(tymczasowaTrasa.size() - 1);
                tymczasowyCzas.remove(tymczasowyCzas.size() - 1);
                return;
            }
        }

        for(int i=0;i<sciezki.size();i++)
        {
            if(start.equals(sciezki.get(i).getPktPoczatkowy())) //przypadek "bo po co sie cofac?". To wylapuje tez postoje, ale postoje sa znajdywane i obslugiwane wczesniej, wiec git.
            {
                int kontrol=0;
                for(int k=0;k<tymczasowaTrasa.size();k++)
                {
                    if(sciezki.get(i).getPktKoncowy().equals(tymczasowaTrasa.get(k).getPktPoczatkowy()) || sciezki.get(i).getPktKoncowy().equals(tymczasowaTrasa.get(k).getPktKoncowy()))
                    {
                        kontrol++;
                        break;
                    }
                }
                if(kontrol != 0)continue;
            }
            if(start.equals(sciezki.get(i).getPktPoczatkowy()) && koniec.equals(sciezki.get(i).getPktKoncowy())) //przypadek ko�cz�cy
            {
                //System.out.println("Ide " + sciezki.get(i));  //debug
                tymczasowaTrasa.add(sciezki.get(i));
                tymczasowyCzas.add(sciezki.get(i).getCzas());

                if(czasCalejTrasy == 0) //zostala wytyczona pierwsza mozliwa trasa
                {
                    for(int j=0;j<tymczasowyCzas.size();j++)
                        czasCalejTrasy += tymczasowyCzas.get(j);
                    trasa.clear(); //na wszelki wypadek
                    trasa.addAll(tymczasowaTrasa);
                    czyMamJuzPorownywacCzas = true;


                    //System.out.println("Znalazlem trase:");
                    //printTrasa();
                }
                else //jakas trasa juz istnieje, wiec sprawdzam czy nowa jest szybsza
                {
                    double temp = 0;
                    for(int j=0;j<tymczasowyCzas.size();j++)
                        temp += tymczasowyCzas.get(j);

                    if(temp < czasCalejTrasy)
                    {
                        czasCalejTrasy = temp;
                        trasa.clear();
                        trasa.addAll(tymczasowaTrasa);

                        //System.out.println("Znalazlem szybsza trase:");
                        //printTrasa();
                    }
                }
                //System.out.println("Wracam sciezka " + tymczasowaTrasa.get(tymczasowaTrasa.size() - 1));  //debug
                tymczasowaTrasa.remove(tymczasowaTrasa.size() - 1);
                tymczasowyCzas.remove(tymczasowyCzas.size() - 1);
                //w tym miejscu byl dodany break, ktory mi zepsu� 2 godziny zycia -.-"
            }
            else if(start.equals(sciezki.get(i).getPktPoczatkowy())) //przypadek zwykly
            {
                //System.out.println("Ide " + sciezki.get(i));  //debug
                tymczasowaTrasa.add(sciezki.get(i));
                tymczasowyCzas.add(sciezki.get(i).getCzas());

                double timer = 0;
                for(int j=0;j<tymczasowyCzas.size();j++)
                    timer += tymczasowyCzas.get(j);

                if(!czyMamJuzPorownywacCzas)
                    znajdzNajszybszaTrase(sciezki, sciezki.get(i).getPktKoncowy(), koniec, tymczasowyCzas);
                else if(timer < czasCalejTrasy)
                    znajdzNajszybszaTrase(sciezki, sciezki.get(i).getPktKoncowy(), koniec, tymczasowyCzas);

                //System.out.println("Wracam sciezka " + tymczasowaTrasa.get(tymczasowaTrasa.size() - 1));  //debug
                tymczasowaTrasa.remove(tymczasowaTrasa.size() - 1);
                tymczasowyCzas.remove(tymczasowyCzas.size() - 1);

            }

        }


    }

    public void printTrasa() {
        for (int a = 0; a < trasa.size(); a++)
            System.out.println(trasa.get(a));
        System.out.println("W czasie: " + czasCalejTrasy); //debug
    }

    public MojaLista getTrasa() {
        return trasa;
    }

    public double getCzasCalejTrasy() {
        return czasCalejTrasy;
    }

    public void setTakiBylPktStartowy(String takiBylPktStartowy) {
        this.takiBylPktStartowy = takiBylPktStartowy;
    }
}
