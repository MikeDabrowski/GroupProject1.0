package piotrek;

/**
 * Created by Piotr Góralewski on 2016-01-02.
 */
public class Node {

    private Sciezka sciezka;
    private Node nextNode;

    public Node(Sciezka sciezka, Node nextNode) {
        this.sciezka = sciezka;
        this.nextNode = nextNode;
    }

    public Node(Sciezka sciezka) {
        this.sciezka = sciezka;
    }

    public Sciezka getSciezka() {
        return sciezka;
    }

    public void setSciezka(Sciezka sciezka) {
        this.sciezka = sciezka;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Node nod) {
        this.nextNode = nod;
    }

    @Override
    public String toString() {
        return "Node{" +
                "sciezka=" + sciezka +
                '}';
    }
}
