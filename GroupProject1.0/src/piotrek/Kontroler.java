package piotrek;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import piotrek.Trasa;

/**
 * Created by Piotr Goralewski on 2015-12-20.
 */
public class Kontroler {

    public static MojaLista daneTestowe = new MojaLista();
    /*public static void przygotujTest()
    {
        daneTestowe.add(new Sciezka("B","C",20,1));
        daneTestowe.add(new Sciezka("C","D",4,1));
        daneTestowe.add(new Sciezka("G","B",12,1));
        daneTestowe.add(new Sciezka("C","J",18,1));
        daneTestowe.add(new Sciezka("F","I",17,1));
        daneTestowe.add(new Sciezka("F","E",10,1));
        daneTestowe.add(new Sciezka("F","G",2,1));
        daneTestowe.add(new Sciezka("E","D",1,1));
        daneTestowe.add(new Sciezka("G","H",5,1));
        daneTestowe.add(new Sciezka("H","C",7,1));
        daneTestowe.add(new Sciezka("E","H",2,1));
        daneTestowe.add(new Sciezka("A","F",1,1));
        daneTestowe.add(new Sciezka("A","B",30,1));

        daneTestowe.add(new Sciezka("B","B",18,1));
        daneTestowe.add(new Sciezka("E","E",50,1));
        daneTestowe.add(new Sciezka("F","F",3,1));
        daneTestowe.add(new Sciezka("G","G",20,1));
    }*/

    public static String randomString(int len)
    {
        char[] str = new char[100];

        for (int i = 0; i < len; i++)
        {
            str[i] = (char) (((int)(Math.random() * 26)) + (int)'A');
        }

        return (new String(str, 0, len));
    }

    private static void zrobLosoweDane(int liczbaDanych)
    {
        int licznik = 0;
        String pktStartowy, pktKoncowy;
        int dlugosc, predkosc, ctrl;
        while(licznik != liczbaDanych)
        {
            //tworze punkty
            pktStartowy = randomString(((int)(Math.random() * 1)) + 1);
            pktKoncowy = randomString(((int)(Math.random() * 1)) + 1);

            //sprawdzam, czy sciezka o takich punktach juz istnieje. Jezeli tak, to jeszcze raz while
            ctrl = 0;
            for(int i=0;i<daneTestowe.size();i++)
            {
                if(daneTestowe.get(i).getPktPoczatkowy().equals(pktStartowy) && daneTestowe.get(i).getPktKoncowy().equals(pktKoncowy))
                {
                    ctrl = 1;
                    continue;
                }
            }
            if(ctrl == 1) continue;

            //tu docieraja tylko unikatowe sciezki
            //System.out.println(licznik);  //debug
            dlugosc = (int)((Math.random() * 30)) + 100;
            predkosc = (int)((Math.random() * 12)) + 1;
            daneTestowe.add(new Sciezka(pktStartowy, pktKoncowy, dlugosc, predkosc));
            System.out.println(daneTestowe.get(daneTestowe.size()-1)); //debug
            licznik++;
            if(licznik == 40000) break;  //dodatkowe zabezpieczenie, ktore nie pozwala na tworzenie zbyt duzej ilosci sciezek
            //np dysponujac tylko punktami o nazwach jednoznakowych("A" "B" itd) mamy mozliwosc stworzyc max ok 650 sciezek. Jezeli uzytkownik bedzie chcial ich wiecej, a nie bedzie tego ifa, to petla sie nigdy nie skonczy.
            // dla ponad 80000 wykonuje sie ponad 4min.
        }
        System.out.println("Przygotowalem " + licznik + " sciezek.");  //debug
    }

    public static void odczyt(File file)
    {
        Czytnik c = new Czytnik();
        try
        {
            daneTestowe = c.odczytajSciezki(file);
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Nie znalazlem takiego pliku!");
        }
        catch(IOException e)
        {
            System.out.println("Cos poszlo nie tak...");
        }
    }


    public static void main(String start, String end,File file,boolean showInputData,boolean takeTime) throws FileNotFoundException{
    
    	//Kontroler.main("a", "b", plikTestowy_24, false,false);
        //main.Kontroler k = new main.Kontroler();
        //przygotujTest(); //tu wstawic inna metode ktora przygotuje ArrayList ze sciezkami. Np odczytane z notatnika
        //zrobLosoweDane(50);
        odczyt(file);
        long time=0,timestop=0;

        Trasa t = new Trasa();
        ArrayList<Double> rekurencja = new ArrayList<>(); //zmienna potrzebna przy rekurencji, na samym poczatku ma byc pusta
        t.setTakiBylPktStartowy("a");
        if(takeTime)
        	time=System.nanoTime();
        t.znajdzNajszybszaTrase(daneTestowe, start, end, rekurencja);
        if(takeTime)
        	timestop=System.nanoTime()-time;

        MojaLista trasa = t.getTrasa();
        //t.printTrasa();

        System.out.println("/==============Nasz  algorytm=============\\");
        System.out.println("Znalazlem trase:");
        for(int i=0;i<trasa.size();i++)
        {
            System.out.println(trasa.get(i));
        }
        System.out.println("Trase pokonalem w czasie: " + t.getCzasCalejTrasy());
        if(takeTime)
        	System.out.println("Czas wyszukiwania(ms): "+java.util.concurrent.TimeUnit.MILLISECONDS.convert(timestop, TimeUnit.NANOSECONDS));
        System.out.println("\\_________________________________________/");


    }
}
