package piotrek;

/**
 * Created by Piotr Góralewski on 2016-01-02.
 */
public class MojaLista {

    private Node sentFirst, sentLast;
    //private int licznikNodow = 0;

    public void add(Sciezka o)
    {
        Node newOne = new Node(o);
        if(this.size() == 0)
        {
            sentFirst = newOne;
            sentLast = newOne;
        }
        else
        {
            sentLast.setNextNode(newOne);
            sentLast = newOne;
        }
        //licznikNodow++;
    }

    public void remove(int index)
    {
        if(index >= this.size() || index < 0)
        {
            throw new IndexOutOfBoundsException("Podany indeks jest za duzy lub za maly");
        }
        else if(this.size() == 0)
        {
            //System.out.println("trololololo"); //debug
            return;
        }
        else if(index == 0 && this.size() == 1)
        {
            sentFirst = null;
            sentLast = null;
        }
        else if(index == 0)
        {
            sentFirst = sentFirst.getNextNode();
        }
        else if(index == this.size() - 1)
        {
            Node przedni = sentFirst;
            for (int i = 0; i < index - 1; i++) {
                przedni = przedni.getNextNode();
            }
            przedni.setNextNode(null);
            sentLast = przedni;
        }
        else
        {
            Node przedni = sentFirst;
            for (int i = 0; i < index - 1; i++) {
                przedni = przedni.getNextNode();
            }
            Node nast = przedni.getNextNode();
            przedni.setNextNode(nast.getNextNode());
            nast.setNextNode(null);
        }

        //licznikNodow--;
    }

    public Node getNode(int index)
    {
        if(index >= this.size() || index < 0)
        {
            throw new IndexOutOfBoundsException("Podany indeks jest za duzy lub za maly");
        }
        Node node = sentFirst;
        for (int i = 0; i < index; i++) {
            node = node.getNextNode();
        }
        return node;
    }

    public Sciezka get(int index)
    {
        Node node = getNode(index);
        return node.getSciezka();
    }

    public int size()
    {
        //return licznikNodow;
        if(sentFirst == null)
            return 0;
        Node nod = sentFirst;
        int bardzoFajnaChwilowaZmiennaPotrzebnaTylkoWJednejPetliATakNaSerioToNieMamBladegoPojeciaJakJaNazwacWiecBedzieTakiBelzebub = 1;
        while(nod.getNextNode() != null)
        {
            nod = nod.getNextNode();
            bardzoFajnaChwilowaZmiennaPotrzebnaTylkoWJednejPetliATakNaSerioToNieMamBladegoPojeciaJakJaNazwacWiecBedzieTakiBelzebub++;
        }
        return bardzoFajnaChwilowaZmiennaPotrzebnaTylkoWJednejPetliATakNaSerioToNieMamBladegoPojeciaJakJaNazwacWiecBedzieTakiBelzebub;
    }

    public void clear()
    {
        sentLast = null;
        sentFirst = null;
        //licznikNodow = 0;
    }

    public void addAll(MojaLista listaDoDodania)
    {
        if(listaDoDodania == null || listaDoDodania.size() == 0)
            return;

        for(int i=0;i<listaDoDodania.size();i++)
        {
            this.add(new Sciezka(listaDoDodania.get(i).getPktPoczatkowy(),listaDoDodania.get(i).getPktKoncowy(),listaDoDodania.get(i).getCzas()));
        }

    }

    @Override
    public String toString() {

        Node node = sentFirst;
        String s = "";
        while(node.getNextNode() != null) {
            s+=node;
            node = node.getNextNode();
        }
        return "MojaLista{" + s + "}";
    }
}
