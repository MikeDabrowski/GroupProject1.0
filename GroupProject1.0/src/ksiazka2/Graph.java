package ksiazka2;

import java.util.ArrayList;
import java.util.Collections;

class Graph{
	private final int MAX_VERTS = 50;//warning!
	private final double INFINITY = Double.POSITIVE_INFINITY;
	private Vertex vertexList[]; // list of vertices
	private double adjMat[][]; // adjacency matrix
	private int nVerts; // current number of vertices
	private int nTree; // number of verts in tree
	public DistPar sPath[]; // array for shortest-path data
	private int currentVert; // current vertex
	private double startToCurrent; // distance to currentVert
// -------------------------------------------------------------
	public Graph(){ // constructor
		vertexList = new Vertex[MAX_VERTS];
		// adjacency matrix
		adjMat = new double[MAX_VERTS][MAX_VERTS];
		nVerts = 0;
		nTree = 0;
		for(int j=0; j<MAX_VERTS; j++) // set adjacency
		for(int k=0; k<MAX_VERTS; k++) // matrix
		adjMat[j][k] = INFINITY; // to infinity
		sPath = new DistPar[MAX_VERTS]; // shortest paths
	} // end constructor
// -------------------------------------------------------------
	public void addVertex(char lab){
		vertexList[nVerts++] = new Vertex(lab);
	}
// -------------------------------------------------------------
	public void addEdge(int start, int end, double weight){
		adjMat[start][end] = weight; // (directed)
	}
// -------------------------------------------------------------
	public void path(boolean showAllPaths){ // find all shortest paths
		int startTree = 0; // start at vertex 0
		vertexList[startTree].isInTree = true;
		nTree = 1; // put it in tree
		// transfer row of distances from adjMat to sPath
		for(int j=0; j<nVerts; j++){
			double tempDist = adjMat[startTree][j];
			sPath[j] = new DistPar(startTree, tempDist);
		}
		/*for (int i = 0; i < adjMat.length; i++) {
			for (int j = 0; j < adjMat.length; j++) {
				if(adjMat[i][j]==Double.POSITIVE_INFINITY)
					System.out.print(adjMat[i][j]+"\t");
				else
					System.out.print(adjMat[i][j]+"\t\t");
			}
			System.out.println();
		}
		System.out.println("Spath");
		for(int i=0;i<7;i++){
			System.out.print(sPath[i].distance+" ");
		}System.out.println("");*/
		// until all vertices are in the tree
		while(nTree < nVerts){
			int indexMin = getMin(); // get minimum from sPath
			double minDist = sPath[indexMin].distance;
			if(minDist == INFINITY){ // if all infinite
			 // or in tree,
				System.out.println("There are unreachable vertices");
				break; // sPath is complete
			}
			else{ // reset currentVert
				currentVert = indexMin; // to closest vert
				startToCurrent = sPath[indexMin].distance;
				// minimum distance from startTree is
				// to currentVert, and is startToCurrent
			}
			// put current vertex in tree
			vertexList[currentVert].isInTree = true;
			nTree++;
			adjust_sPath(); // update sPath[] array
		} // end while(nTree<nVerts)
		if(showAllPaths)
			displayPaths(); // display sPath[] contents
		nTree = 0; // clear tree
		for(int j=0; j<nVerts; j++)
		vertexList[j].isInTree = false;
	} // end path()
// -------------------------------------------------------------
	public int getMin(){ // get entry from sPath
	// with minimum distance
	double minDist = INFINITY; // assume minimum
	int indexMin = 0;
	for(int j=1; j<nVerts; j++){ // for each vertex,
	// if it's in tree and
		if( !vertexList[j].isInTree && // smaller than old one
				sPath[j].distance < minDist )
				{
				minDist = sPath[j].distance;
				indexMin = j; // update minimum
				}
				} // end for
		return indexMin; // return index of minimum
	} // end getMin()
// -------------------------------------------------------------
	public void adjust_sPath()	{
		// adjust values in shortest-path array sPath
		int column = 1; // skip starting vertex
		while(column < nVerts) // go across columns
		{
		// if this column's vertex already in tree, skip it
		if( vertexList[column].isInTree )		{
			column++;
			continue;
		}
		// calculate distance for one sPath entry
		// get edge from currentVert to column
		double currentToFringe = adjMat[currentVert][column];
		// add distance from start
		double startToFringe = startToCurrent + currentToFringe;
		// get distance of current sPath entry
		double sPathDist = sPath[column].distance;
		// compare distance from start with sPath entry
		if(startToFringe < sPathDist) // if shorter,
		{ // update sPath
		sPath[column].parentVert = currentVert;
		sPath[column].distance = startToFringe;
		}
		column++;
		} // end while(column < nVerts)
	} // end adjust_sPath()
// -------------------------------------------------------------
	public void displayPaths()
	{
		for(int j=0; j<nVerts; j++) // display contents of sPath[]
		{
			System.out.print(vertexList[j].label + "="); // B=
			if(sPath[j].distance == INFINITY)
				System.out.print("inf"); // inf
			else
				System.out.print(sPath[j].distance); // 50
			char parent = vertexList[ sPath[j].parentVert ].label;
			System.out.print("(" + parent + ") "); // (A)
			}
			System.out.println("");
		}
	// -------------------------------------------------------------
	public DistPar[] getSPath(){
		return sPath;
	}
	// -------------------------------------------------------------
	public void getCost(int indexOfEnd){
		double cost=sPath[indexOfEnd].distance;/*
		System.out.print("\n"+"vertexListLabels: ");
		for(int i=0;i<indexOfEnd;i++)
			System.out.print(vertexList[i].label+" ");
		System.out.print("\n"+"Spath: ");
		for(int i=0;i<indexOfEnd;i++)
			System.out.print(sPath[i].distance+" ");
		*/
		System.out.println(""+cost);
	}
	// -------------------------------------------------------------
	public ArrayList<String> getComplPath(int indexOfEnd){
		ArrayList<String> complPa=new ArrayList<>();
		int cos=indexOfEnd;
		complPa.add(String.valueOf(vertexList[indexOfEnd].label));
		while(cos!=0) {
			complPa.add(String.valueOf(vertexList[sPath[cos].parentVert].label));
			cos=getParent(cos);			
		}
		Collections.reverse(complPa);
		return complPa;
	}
	// -------------------------------------------------------------
	public int getParent(int ix){
		return sPath[ix].parentVert;
	}
} // end class Graph
				////////////////////////////////////////////////////////////////