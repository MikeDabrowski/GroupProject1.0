package ksiazka2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

//path.java
//demonstrates shortest path with weighted, directed graphs
//to run this program: C>java PathApp


public class PathApp {
	static ArrayList<String> whatVertices = new ArrayList<>();
	public static void main(String start, String end,File file,boolean showInputData,boolean takeTime) throws FileNotFoundException{
		Graph theGraph=makeGraph(file,start,end);
		System.out.println("\n/====Dijkstra's Algrithm from The Book====\\");
		if(showInputData)
			System.out.println("Shortest paths");
		theGraph.path(showInputData);
		
		System.out.print(theGraph.getComplPath(whatVertices.indexOf(end)).toString());
		System.out.print(" in time: ");
		theGraph.getCost(whatVertices.indexOf(end));
		 
		System.out.println("\\_________________________________________/\n");
		//theGraph.displayPaths();
	} // end main()
	
	private static Graph makeGraph(File file,String start,String end) throws FileNotFoundException{
		Graph theGraph = new Graph();
		whatVertices = new ArrayList<>();//will store found vertices
		Scanner inputFile = new Scanner(file);
		String znak;
		while(inputFile.hasNext()){
			znak=inputFile.next();
			//check for only Strings that are not numbers
			if(!Character.isDigit(znak.charAt(0))){
				if(whatVertices.contains(znak)==false){
					whatVertices.add(znak);
				}
			}
		}
		inputFile.close();
//		whatVertices.remove(whatVertices.indexOf(start));
		Collections.sort(whatVertices);
		Collections.swap(whatVertices, 0, whatVertices.indexOf(start));
		//add start
//		theGraph.addVertex(start.charAt(0));// 0 (start)
		for(int i=0;i<whatVertices.size();i++){
			theGraph.addVertex(whatVertices.get(i).charAt(0)); 
		}
//		whatVertices.add(start);
//		Collections.sort(whatVertices);
		ArrayList<String> delays=new ArrayList<>();
		ArrayList<Double> delays3=new ArrayList<>();
		Scanner inputFile3 = new Scanner(file);
		String d1,d2;
		int d3,d4;
		double d5;
		while(inputFile3.hasNext()){
			d1=inputFile3.next();
			d2=inputFile3.next();
			d3=inputFile3.nextInt();
			d4=inputFile3.nextInt();
			d5=(double)d3/(double)d4;
			if(d1.equals(d2)){
				delays.add(d1);
				delays3.add(d5);
			}
		}
		inputFile3.close();
		Scanner inputFile2 = new Scanner(file);
		String  t1,t2;
		int i1,i2;
		double tim;
		while(inputFile2.hasNext()){
			t1=inputFile2.next();
			t2=inputFile2.next();
			i1=inputFile2.nextInt();
			i2=inputFile2.nextInt();
			tim=(double)i1/(double)i2;
			if(delays.contains(t2)&&!t1.equals(t2)&&!t2.equals(end)){
				tim=tim+delays3.get(delays.indexOf(t2));
			}
			theGraph.addEdge(whatVertices.indexOf(t1), whatVertices.indexOf(t2), tim);
		}
		inputFile2.close();
		return theGraph;		
	}
} // end class PathApp
////////////////////////////////////////////////////////////////
