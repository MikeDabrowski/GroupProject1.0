package moje3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import moje3.Graph;

public class DATest {

	private static List<Vertex> nodes;
	private static List<Edge> edges;
	private static ArrayList<String> whatVertices;
	
	public static void main(String start, String end,File file,boolean showInputData, boolean takeTime) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Graph graph =readFromFile(file, showInputData);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		long time=System.nanoTime();
		dijkstra.execute(nodes.get(whatVertices.indexOf(start)));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(whatVertices.indexOf(end)));
		long endTime=System.nanoTime()-time;
		
		System.out.println("\n/===========Dijkstra's Algrithm===========\\");
		for (int i = 0; i < path.size(); i++) {
			if(i!=path.size()-1)
				System.out.print(path.get(i).getName()+" -> ");
			else
				System.out.print(path.get(i).getName());
			//pathCost=edges.get(index)
		}
		System.out.println(" in time: "+dijkstra.getShortestDistance(nodes.get(whatVertices.indexOf(end))));
		if(takeTime)
			System.out.println("Czas wyszukiwania: "+endTime);
		System.out.println("\\_________________________________________/\n");
	}

	private static Graph readFromFile(File file,boolean showInputData) throws FileNotFoundException{
		nodes= new ArrayList<Vertex>();
		edges= new ArrayList<Edge>();
		//a b 4 1
		
		whatVertices = new ArrayList<>();//will store found vertices
		Scanner inputFile = new Scanner(file);
		String znak;
		while(inputFile.hasNext()){
			znak=inputFile.next();
			//check for only Strings that are not numbers
			if(!Character.isDigit(znak.charAt(0))){
				if(whatVertices.contains(znak)==false){
					whatVertices.add(znak);
				}
			}
		}
		Collections.sort(whatVertices);
		if(showInputData)
			System.out.println("Nodes:");
		for (int i = 0; i < whatVertices.size(); i++) {
			nodes.add(new Vertex("Node_" + i, whatVertices.get(i)));
			if(showInputData)
				System.out.println("NodeID: "+nodes.get(i).getId()+" NodeName: "+nodes.get(i).getName());
		}
		//count lines
		
		Scanner in = new Scanner(file);
		int temp3,temp4;
		int source,destination,weight;
		double temp5;
		int i=0;
		while(in.hasNext()){
	    	source=whatVertices.indexOf(in.next());
	    	destination=whatVertices.indexOf(in.next());
	    	temp3=in.nextInt();
    		temp4=in.nextInt();
	    	//temp5=(double)temp3/(double)temp4;
	    //	System.out.println(source+" "+destination+" "+temp3);
	    	edges.add(new Edge("Edge_"+i, nodes.get(source), nodes.get(destination), temp3));
	    	//Graph2[i]=new Graph.Edge(temp1,temp2,temp5);
	    	//i++;
	    }
		in.close();
			
		Graph graph = new Graph(nodes, edges);
		return graph;
	}
	
}
