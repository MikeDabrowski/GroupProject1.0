package grpr;

public class Path {
	private char startPoint;
	private char endPoint;
	private int velocity;
	private int distance;
	private boolean bidirectional; //if path is bidirectional
	private double time;
	
	public Path(char startPoint,char endPoint, int distance,int velocity, boolean bidirectional){
		this.startPoint=startPoint;
		this.endPoint=endPoint;
		this.velocity=velocity;
		this.distance=distance;
		this.bidirectional=bidirectional;		
	}
	
	public double getTime(){
		time=(double)(distance)/(double)(velocity);		
		return time;
	}
	
	public void setStartPoint(char startPoint){
		this.startPoint=startPoint;
	}
	
	public void setEndPoint(char endPoint){
		this.endPoint=endPoint;
	}
	
	public void setVelocity(int velocity){
		this.velocity=velocity;
	}
	
	public void setDistance(int distance){
		this.distance=distance;
	}
	
	public int getStartPoint(){
		return distance;
	}
	
	public int getEndPoint(){
		return distance;
	}
	
	public int getVelocity(){
		return velocity;
	}
	
	public int getDistance(){
		return distance;
	}
}
