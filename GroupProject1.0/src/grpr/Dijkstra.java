package grpr;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

import moje.DaneTestowe;

public class Dijkstra {
	/*
	 * Algorithm is derived from Wikipedia section 'Using a priority queue'. 
	 * This implementation finds the single path from a source to all reachable vertices. 
	 * Building the graph from a set of edges takes O(E log V) for each pass. 
	 * Vertices are stored in a TreeSet (self-balancing binary search tree) instead of a PriorityQueue 
	 * (a binary heap)in order to get O(log n) performance for removal of any element, not just the head. 
	 * Decreasing the distance of a vertex is accomplished by removing it from the tree and later 
	 * re-inserting it. 
	 */

	public static void main(String start, String end,File file,boolean showInputData,boolean printAllPaths,boolean takeTime) throws FileNotFoundException {
		Graph.Edge[] GRAPH1=DaneTestowe.przygotujTestD2(file,showInputData);
		Graph g = new Graph(GRAPH1);
		System.out.println("\n/==========Dijkstra's Algrithm 1=========\\");
		long time=0, timeend=0;
		if(takeTime==true){
			time=System.nanoTime();
		}
	    g.dijkstra(start);
	    
	    
	    g.printPath(end);
	    if(printAllPaths==true){
	    	System.out.println("More paths (usually wrong)");
	    	g.printAllPaths();
	    }
	    if(takeTime==true){
	    	timeend=System.nanoTime()-time;
	    	System.out.println("Czas wyszukiwania: "+java.util.concurrent.TimeUnit.MILLISECONDS.convert(timeend, TimeUnit.NANOSECONDS));
	    }
	    System.out.println("\\_________________________________________/\n");
	}
}
	 