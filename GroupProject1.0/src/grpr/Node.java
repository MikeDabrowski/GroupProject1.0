package grpr;

public class Node {
	private String name;
	private int x;
	private int y;
	private double pause;
	
	public Node(String name, int x, int y, double pause) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.pause = pause;
     }
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}
