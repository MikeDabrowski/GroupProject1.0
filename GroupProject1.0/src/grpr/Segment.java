package grpr;

public class Segment implements Comparable<Segment>{
	public final double dist;
	public final int velo;
	public double time;
	
	public Segment(Node n1,Node n2, int velo) {
		double x2=(n1.getX()-n2.getX())*(n1.getX()-n2.getX());
		double y2=(n1.getY()-n2.getY())*(n1.getY()-n2.getY());
		System.out.println("x2,y2 "+x2+' '+y2);
		this.dist=Math.sqrt(x2+y2);
		this.velo = velo;
		time=dist/(double)(velo);
	}	
	
	public int compareTo(Segment other) {
        return Double.compare(time, other.time);
     }
	
	public double getDist(){
		return dist;
	}
	public double getTime(){
		return time;
	}
}
