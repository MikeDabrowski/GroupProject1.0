package grpr;
import grpr.Path;

public class Route {
	private double totalTime=0;
	
	public double calculateTime(){
		
		Path path1=new Path('A','B',100,50,false);
		Path path2=new Path('B','B',120,50,false);
		Path path3=new Path('B','C',110,50,false);
		Path path4=new Path('C','D',90,50,false);
		Path[] arrayOfPoints={path1,path2,path3,path4};
		
		
		int i=0;
		while (i == arrayOfPoints.length+1){
			totalTime=totalTime+arrayOfPoints[i].getTime();
			i++;
		}
		return totalTime;
	}
}
