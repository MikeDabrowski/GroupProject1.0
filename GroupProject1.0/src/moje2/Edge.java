package moje2;

public class Edge implements Comparable<Edge>{
	private String startPoint,endPoint;
	private Vertex SP,EP;
	private double time;
	private int distance,velocity;
	
	public Edge(String S, String E, int dist, int velo)
    {
		startPoint = S;
		endPoint = E;
        time = (double)dist / (double)velo;
        velocity=velo;
        distance=dist;
    }
	public Edge(Vertex SV,Vertex EV, int dist, int velo)
    {
		SP = SV;
		EP = EV;
        time = (double)dist / (double)velo;
        velocity=velo;
        distance=dist;
    }

    public String toString2()
    {
        if(startPoint.equals(endPoint))
            return "Postoj w punkcie " + startPoint + "   (time postoju = " + time + ")\n";
        else
            return startPoint + " -> " + endPoint + "               (time podrozy = " + time + ")\n";
        //return startPoint + " -> " + endPoint + " w timeie: " + time;
    }
    public String toString()
    {
        if(startPoint.equals(endPoint))
            return "wait in " + startPoint + " for=" + time + " ";
        else
            return startPoint + " -> " + endPoint + " in = " + time + " ";
        //return startPoint + " -> " + endPoint + " w timeie: " + time;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public double getTime() {
        return time;
    }
    public int getDistance(){
    	return distance;
    }
    public int getVelocity(){
    	return velocity;
    }
    @Override
	public int compareTo(Edge e) {
		if(this.distance>e.distance){
			return 1;
		}
		else if(this.distance<e.distance)
			return -1;
		else
			return 0;
	}
}
