package moje2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class DijkstraMoje {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		String start="a",current,end="g";
		int order;
		File plik1 = new File("src/moje/p.txt");
		ArrayList<Vertex> vertices=makeVertices(plik1, false);
		ArrayList<String> next;
		ArrayList<Vertex> nextVals=new ArrayList<>();
		ArrayList<Edge> edges=makeEdges(plik1, false);
		ArrayList<Edge> nextEdges;
		
		AdjacencyMatrix am=new AdjacencyMatrix(plik1,true);
		HashMap<String, Vertex> hm = new HashMap();
		for (int i = 0; i < vertices.size(); i++) {
			hm.put(vertices.get(i).getName(), vertices.get(i));			
		}
		//get first vertex
		order=1;
		hm.get(start).setOrder(order);
		order++;
		hm.get(start).setFinalValue(0);
		current=start;
		//loop start
			System.out.println("LOOP 1ST PASS");
			//get all vertices that can be reached from the first one
			nextEdges=am.getNextPossibleEdges(hm.get(current));
			//System.out.println(nextEdges.toString());
			Collections.sort(nextEdges);
			System.out.println(nextEdges.toString());
			//find smallest working value and save it to vertex of origin, set it as next in path
			System.out.print("smallest : "+nextEdges.get(0).getEndPoint()+", reached in: ");
			hm.get(nextEdges.get(0).getEndPoint()).setFinalValue(nextEdges.get(0).getDistance());
			System.out.print(hm.get(nextEdges.get(0).getEndPoint()).getFinalValue()+", order set to: ");
			//set order of vertex with smallest working value to next one
			hm.get(nextEdges.get(0).getEndPoint()).setOrder(order++);
			System.out.println(hm.get(nextEdges.get(0).getEndPoint()).getOrder());
			current=hm.get(nextEdges.get(0).getEndPoint()).getName();
			//remove selected path from remaining edges
			nextEdges.remove(0);
			System.out.println(nextEdges.toString());
		//loop end
			/*next=am.getNextPossibleVertices(hm.get(current));
			System.out.println(am.getNextPossibleVertices(hm.get(start)).toString());
			//assign corresponding vertices distances
			for (int i = 0; i < next.size(); i++) {
				hm.get(next.get(i)).addWorkingValue(am.getTimeBetween(start, next.get(i)));
				//System.out.println("hm.get("+next.get(i)+").addWrokingVal("+am.getTimeBetween(start, next.get(i))+") i co: "+hm.get(next.get(i)).getWorkingValues().get(0));
				nextVals.add(new Vertex(next.get(i),am.getTimeBetween(start, next.get(i))));//add to sort later
				//System.out.println(next.get(i)+" "+am.getTimeBetween(start, next.get(i))+" d "+nextVals.get(i).getName()+" get smallestDist "+nextVals.get(i).getSmallestDist());
			}
			//sort, pick smallest
			Collections.sort(nextVals);
			for(int i=0;i<nextVals.size();i++)
				System.out.println(nextVals.get(i).getName());
			//find smallest working value and save it to vertex of origin, set it as next in path
			hm.get(nextVals.get(0).getName()).setFinalValue(nextVals.get(0).getSmallestDist());
			System.out.println(hm.get(nextVals.get(0).getName()).getFinalValue());
			//set order of vertex with smallest working value to next one
			hm.get(nextVals.get(0).getName()).setOrder(order);
			System.out.println(hm.get(nextVals.get(0).getName()).getOrder());
			current=hm.get(nextVals.get(0).getName()).getName();
			nextVals.remove(0);//remove from possible path
*/		//loop end
		//loop 2nd pass
			System.out.println("LOOP 2ND PASS");
			//get all vertices that can be reached from the first one
			nextEdges.addAll(am.getNextPossibleEdges(hm.get(current)));
			//System.out.println(nextEdges.toString());
			Collections.sort(nextEdges);
			System.out.println(nextEdges.toString());
			//find smallest working value and save it to vertex of origin, set it as next in path
			System.out.print("smallest : "+nextEdges.get(0).getEndPoint()+", reached in: ");
			hm.get(nextEdges.get(0).getEndPoint()).setFinalValue(nextEdges.get(0).getDistance());
			System.out.print(hm.get(nextEdges.get(0).getEndPoint()).getFinalValue()+", order set to: ");
			//set order of vertex with smallest working value to next one
			hm.get(nextEdges.get(0).getEndPoint()).setOrder(order++);
			System.out.println(hm.get(nextEdges.get(0).getEndPoint()).getOrder());
			current=hm.get(nextEdges.get(0).getEndPoint()).getName();
			//remove selected path from remaining edges
			nextEdges.remove(0);
			System.out.println(nextEdges.toString());
			
			/*System.out.println("LOOP 2ND PASS");
			//get all vertices that can be reached from the first one
			next=am.getNextPossibleVertices(hm.get(current));
			System.out.println(next.toString());
			//assign corresponding vertices distances
			double nextTime;
			for (int i = 0; i < next.size(); i++) {
				nextTime=am.getTimeBetween(current, next.get(i));
				hm.get(next.get(i)).addWorkingValue(nextTime+hm.get(current).getFinalValue());
				System.out.println("hm.get("+next.get(i)+").addWrokingVal("+am.getTimeBetween(current, next.get(i))+") i co: "+hm.get(next.get(i)).getWorkingValues().get(0));
				nextVals.add(new Vertex(next.get(i),am.getTimeBetween(current, next.get(i))));//add to sort later
				System.out.println(next.get(i)+" time between ("+current+", "+next.get(i)+")= "+am.getTimeBetween(current, next.get(i))+" d "+nextVals.get(i).getName()+" get smallestDist "+nextVals.get(i).getSmallestDist());
			}
			//sort, pick smallest
			Collections.sort(nextVals);
			for(int i=0;i<nextVals.size();i++)
				System.out.println(nextVals.get(i).getName());
			//find smallest working value and save it to vertex of origin, set it as next in path
			hm.get(nextVals.get(0).getName()).setFinalValue(nextVals.get(0).getSmallestDist());
			System.out.println(hm.get(nextVals.get(0).getName()).getFinalValue());*/
			
			
		//LOOP 2 end
		//System.out.println(hm.get(4).getName());
		
		//double[][] adjacencyMatrix=prepAdjMatrix(plik1,false);
		//System.out.println(vertices.get(3).getName());
		//System.out.println(am.getNextPossibleVertices(vertices.get(3)).toString());
		
	}
	/*
 	a b 4 1
	a d 7 1
	a c 3 1
	b d 1 1
	b f 4 1
	c d 3 1
	c e 5 1
	d f 2 1
	d g 7 1
	d e 2 1
	e g 2 1
	f g 4 1
 */
	public static double[][] prepAdjMatrix(File file, boolean showMatrix) throws FileNotFoundException{
		//count vertices in file
		ArrayList<String> whatVertices = new ArrayList<>();//will store found vertices
		Scanner inputFile = new Scanner(file);
		String znak;
		while(inputFile.hasNext()){
			znak=inputFile.next();
			//check for only Strings that are not numbers
			if(!Character.isDigit(znak.charAt(0))){
				if(whatVertices.contains(znak)==false){
					whatVertices.add(znak);
				}
			}
		}
		inputFile.close();
		//sort
		Collections.sort(whatVertices);
				/*debug test
				System.out.println(whatVertices.size());
				for(String counter : whatVertices){
					System.out.println(counter);
				}*/
		//whatVertices now stores in alphabetical order all existing vertices and its length is how many of them are 
		
		//count lines in file to get size of adjacency matrix
		Scanner inputFile2 = new Scanner(file);
		int lines=0;
		while(inputFile2.hasNext()){
			lines++;
			inputFile2.nextLine();
		}
		inputFile2.close();
		
		double[][] adjacencyMatrix=new double[whatVertices.size()][whatVertices.size()];//how many connections has each node
		//fill with max value (infinity like) to show that this is not connection
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				adjacencyMatrix[i][j]=Double.POSITIVE_INFINITY;
			}
		}
		//take one from whatVertices and check how many times it is in endV
		//RETHINK THIS SHIT
		
		ArrayList<Edge> edges=makeEdges(file,false);//get edges list
		
		//System.out.println((whatVertices.toString().indexOf("a")-1)/3);//find index of given element
		
		
		//fill matrix adjMat[indexOfEachStartVertex][indexOfEachEndVertex]=time between them
		for (int i = 0; i < edges.size(); i++) {
			adjacencyMatrix[(whatVertices.toString().indexOf(edges.get(i).getStartPoint())-1)/3]
					   [(whatVertices.toString().indexOf(edges.get(i).getEndPoint())-1)/3]=edges.get(i).getTime();
		}

		if(showMatrix==true){
			//debug show this matrix
			//top axis
			System.out.print(" \t");
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				System.out.print(whatVertices.get(j)+"\t\t");
			}
			System.out.println();
			//matrix and left axis
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				System.out.print(whatVertices.get(j)+"\t");
				for (int j2 = 0; j2 < adjacencyMatrix.length; j2++) {
					if(adjacencyMatrix[j][j2]==Double.POSITIVE_INFINITY)
						System.out.print(adjacencyMatrix[j][j2]+"\t");
					else
						System.out.print(adjacencyMatrix[j][j2]+"\t\t");
				}
				System.out.println();
			}
		}
		return adjacencyMatrix;
	}

	private static int howManyEncounters(ArrayList<String> list, String strToCheck){//just check how many string occurs in startV
		int encounters=0;
		for (String s : list) {
	        if (s.equals(strToCheck)) {
	          encounters++;
	        }
	    }
		return encounters; 
	}
	
	public static ArrayList<Edge> makeEdges(File file, boolean showEdges) throws FileNotFoundException{
		ArrayList<Edge> edges=new ArrayList<>();
		Scanner inputFile3=new Scanner(file);
		while(inputFile3.hasNext()){
			edges.add(new Edge(inputFile3.next(), inputFile3.next(), inputFile3.nextInt(), inputFile3.nextInt()));
		}
		inputFile3.close();
		if(showEdges==true){
			for (int i = 0; i < edges.size(); i++) {
				System.out.println(edges.get(i).toString());
			}
		}
		return edges;
	}
	
	private static ArrayList<Vertex> makeVertices(File file, boolean showVertices) throws FileNotFoundException{
		//count vertices in file
		ArrayList<String> whatVertices = new ArrayList<>();//will store found vertices
		ArrayList<Vertex> vertices = new ArrayList<>();//will be output
		Scanner inputFile = new Scanner(file);
		String znak;
		while(inputFile.hasNext()){
			znak=inputFile.next();
			//check for only Strings that are not numbers
			if(!Character.isDigit(znak.charAt(0))){
				if(whatVertices.contains(znak)==false){
					whatVertices.add(znak);
				}
			}
		}
		inputFile.close();
		//sort
		Collections.sort(whatVertices);
		//put this into vertices form
		for (int i = 0; i < whatVertices.size(); i++) {
			vertices.add(new Vertex(whatVertices.get(i)));
		}
		
		if(showVertices==true){
			for (int i = 0; i < vertices.size(); i++) {
				System.out.println(vertices.get(i).getName());
			}
		}
		
		return vertices;
	}
}
