package moje2;

import java.util.ArrayList;
import java.util.Collections;

public class Vertex implements Comparable<Vertex>{
	//Vertex fields
	private String vertexName;
	private int index;//index in whatVertices table
	private int order;
	private double finalValue;//distance or time
	private double smallestDist;
	private ArrayList<Double> workingValues=new ArrayList<>();
	
	public Vertex(String name){
		vertexName=name;
	}
	public Vertex(String name,double value){
		vertexName=name;
		workingValues.add(value);
		Collections.sort(workingValues);
	}
	public void setIndex(int i){
		index=i;
	}
	public void setOrder(int o){
		order=o;
	}
	public void setFinalValue(double finalV){
		finalValue=finalV;
	}
	public void addWorkingValue(double value){
		workingValues.add(value);
		Collections.sort(workingValues);
	}
	/*public void setSmallestDist(int dist){
		smallestDist=dist;
	}*/
	public int getIndex(){
		return index;
	}
	public String getName(){
		return vertexName;
	}
	public int getOrder(){
		return order;
	}
	public double getFinalValue(){
		return finalValue;
	}
	public ArrayList<Double> getWorkingValues(){
		return workingValues;
	}
	public double getSmallestDist(){
		double sum1=0;
		for(int i=0;i<workingValues.size();i++){
			sum1=sum1+workingValues.get(i);
		}
		smallestDist=sum1;
		return sum1;
	}
	@Override
	public boolean equals(Object object){
		boolean sameSame=false;
		if(object != null && object instanceof Vertex){
			sameSame=this.vertexName==((Vertex)object).vertexName;
		}
		return sameSame;
	}
	@Override
	public int compareTo(Vertex o) {
		// TODO Auto-generated method stub
		double sum1=0,sum2=0;
		for(int i=0;i<this.workingValues.size();i++){
			sum1=sum1+this.workingValues.get(i);
		}
		for(int i=0;i<o.workingValues.size();i++){
			sum2=sum2+o.workingValues.get(i);
		}
		if(sum1>sum2){
			return 1;
		}
		else if(sum1<sum2)
			return -1;
		else
			return 0;
	}
}
