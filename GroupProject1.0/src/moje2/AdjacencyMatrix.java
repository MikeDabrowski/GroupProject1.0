package moje2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AdjacencyMatrix {
	private double[][] adjacencyMatrix;
	private ArrayList<String> whatVertices;
	private ArrayList<Edge> edges;
	public AdjacencyMatrix(File file, boolean showMatrix) throws FileNotFoundException{
		adjacencyMatrix=prepAdjMatrix(file, showMatrix);
		
	}
	public double[][] prepAdjMatrix(File file, boolean showMatrix) throws FileNotFoundException{
		//count vertices in file
		whatVertices = new ArrayList<>();//will store found vertices
		Scanner inputFile = new Scanner(file);
		String znak;
		while(inputFile.hasNext()){
			znak=inputFile.next();
			//check for only Strings that are not numbers
			if(!Character.isDigit(znak.charAt(0))){
				if(whatVertices.contains(znak)==false){
					whatVertices.add(znak);
				}
			}
		}
		inputFile.close();
		//sort
		Collections.sort(whatVertices);
				/*debug test
				System.out.println(whatVertices.size());
				for(String counter : whatVertices){
					System.out.println(counter);
				}*/
		//whatVertices now stores in alphabetical order all existing vertices and its length is how many of them are 
		
		//count lines in file to get size of adjacency matrix
		Scanner inputFile2 = new Scanner(file);
		int lines=0;
		while(inputFile2.hasNext()){
			lines++;
			inputFile2.nextLine();
		}
		inputFile2.close();
		
		double[][] adjacencyMatrix=new double[whatVertices.size()][whatVertices.size()];//how many connections has each node
		//fill with max value (infinity like) to show that this is not connection
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				adjacencyMatrix[i][j]=Double.POSITIVE_INFINITY;
			}
		}
		//take one from whatVertices and check how many times it is in endV
		//RETHINK THIS SHIT
		
		edges=DijkstraMoje.makeEdges(file,false);//get edges list
		
		//System.out.println((whatVertices.toString().indexOf("a")-1)/3);//find index of given element
		
		
		//fill matrix adjMat[indexOfEachStartVertex][indexOfEachEndVertex]=time between them
		for (int i = 0; i < edges.size(); i++) {
			adjacencyMatrix[(whatVertices.toString().indexOf(edges.get(i).getStartPoint())-1)/3]
					   [(whatVertices.toString().indexOf(edges.get(i).getEndPoint())-1)/3]=edges.get(i).getTime();
		}

		if(showMatrix==true){
			//debug show this matrix
			//top axis
			System.out.print(" \t");
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				System.out.print(whatVertices.get(j)+"\t\t");
			}
			System.out.println();
			//matrix and left axis
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				System.out.print(whatVertices.get(j)+"\t");
				for (int j2 = 0; j2 < adjacencyMatrix.length; j2++) {
					if(adjacencyMatrix[j][j2]==Double.POSITIVE_INFINITY)
						System.out.print(adjacencyMatrix[j][j2]+"\t");
					else
						System.out.print(adjacencyMatrix[j][j2]+"\t\t");
				}
				System.out.println();
			}
		}
		return adjacencyMatrix;
	}

	public double[][] getAMatrix(){
		return adjacencyMatrix;
	}
	
	public ArrayList<String> getNextPossibleVertices(Vertex origin){
		ArrayList<String> next=new ArrayList<>();
		int iOE=((whatVertices.toString().indexOf(origin.getName())-1)/3);
		//System.out.println(iOE);//find index of given element
		for (int i = 0; i < adjacencyMatrix.length; i++) {
			if(adjacencyMatrix[iOE][i]!=Double.POSITIVE_INFINITY){
				next.add(whatVertices.get(i));
			}
		}
		//System.out.println(next.toString());
		return next;
	}
	public ArrayList<Edge> getNextPossibleEdges(Vertex origin){
		ArrayList<Edge> nextPE=new ArrayList<>();
		for (int i = 0; i < edges.size(); i++) {
			if(edges.get(i).getStartPoint().equals(origin.getName())){
				nextPE.add(edges.get(i));
			}
		}
		return nextPE;
	}
	
	public double getTimeBetween(String first,String second){
		double time;
		time=adjacencyMatrix[whatVertices.indexOf(first)][whatVertices.indexOf(second)];		
		return time;
	}
}
