package moje;

import moje.Edge;
import java.util.ArrayList;
import java.util.LinkedList;

public class Vertex {

    private int number;    //Cardinal number of Vertex
    private LinkedList<Edge> edge = new LinkedList<Edge>();    //List of edges outgoing from this vertex
 
    private Vertex(){}
 
    //Cardinal number setup========================
    public Vertex(int i)
    {
        number = i;
    }
 
    public String toString()
    {
        if(edge.size() == 0)
            return "";
 
        String out = new String();
        for(Edge e : edge)
            out = out + e.toString() + "\n";
        return out;
    }
    public int getNumber() 
    {
        return number;
    }
 
    
    //Returns copy of list of edges outgoing from this vertex
    public LinkedList<Edge> getEdgeList()
    {
        return new LinkedList<Edge>(edge);
    }
	//remove edges leading to i vertex
    public void removeEdge(int i){
    	int e=edge.size()-1;//how many edges to check
    	while(e>=0){//check all edges
    		if(edge.get(e).getEnd().getNumber()==i)
    			edge.remove(e);
    		e--;
    	}
    }
    //add edge
    public void addEdge(Edge e){
    	edge.add(e);
    }
    //return edge leading to  vertex of cardinal number n (if exists)
    public Edge getEdge(int n){
    	for(Edge e:edge){
    		if(e.getEnd().getNumber()==n)
    			return e;
    	}
	return null;
    }
    //return n-th edge outgoing from vertex (if exists)
    public Edge getEdgeAt(int n){
    	if(n>=0&& n<edge.size())
    		return edge.get(n);
    	else
    		return null;
    }
    
}