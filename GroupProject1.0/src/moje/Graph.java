package moje;

import java.util.LinkedList;
import moje.Edge;
import moje.Vertex;

public class Graph
{
    private LinkedList<Vertex> vertex = new LinkedList<Vertex>();    //list of vertices
    private int vertexCount;            
    /*
     * tells how many vertices the graph has, (null is an empty vertex)
     * vertexCount never decreases, even after deleting all vertices, because deleting is just replacing 
     * existing one with null.
     */
    
    public Graph()//make empty graph
    {
        vertexCount = 0;
    }
 
    public Graph(int n)//make graph with n vertices
    {
        for(int i=0; i<n; i++)
            vertex.add(new Vertex(i));
 
        vertexCount = n;
    }
 
    public String toString()
    {
        String out = new String();
        for(Vertex v : vertex)
            if(v != null && v.toString() != "")
                out = out + v.toString() + "\n";
        return out;
    }
 
    //add new vertex
    public void addVertex()
    {
        vertex.add( new Vertex(vertexCount) );
        vertexCount++;
    }
    
    //remove vertex n,and all its edges
    public void removeVertex(int n)
    {
        if( n<vertexCount )//check if valid edge
        {
            vertex.set(n, null);//set this vertex to null
            for(Vertex v : vertex)//iterate over an array of edges to remove edges
                if(v != null)
                    v.removeEdge(n);
        }
    }
 
    //remove vertex v and all its edges will be broken
    public void removeVertex(Vertex v)
    {
        removeVertex(v.getNumber());
    }
 
    //add edge
    public void addEdge(Edge e)
    {
        vertex.get( e.getBegin().getNumber() ).addEdge( e );
    }
 
    //add edge between b and e, with weight w, if b or e doesnt exist edge is not created
    public void addEdge(int b, int e, double w)
    {
        if( b>=0 && b<=vertexCount && e>=0 && e<=vertexCount && vertex.get(b)!=null && vertex.get(e)!=null)
            addEdge( new Edge(vertex.get(b), vertex.get(e), w) ); 
    }


    //remove edge b>e if exists 
    public void removeEdge(int b, int e)
    {
        if( b>=0 && b<=vertexCount && vertex.get(b)!=null)
            vertex.get(b).removeEdge(e);
    }
 
    //remove edge
    public void removeEdge(Edge e)
    {
        if(e.getBegin() == null) return;
 
        int b = e.getBegin().getNumber();
        if( b>=0 && b<=vertexCount && vertex.get(b)!=null)
            vertex.get(b).removeEdge( e.getEnd().getNumber() );
    }
    
    //returns vertex n 
    public Vertex getVertex(int n)
    {
        return vertex.get(n);
    }
 
	//return copy of list with vertices
    public LinkedList<Vertex> getVertexList()
    {
        return new LinkedList<Vertex>(vertex);
    }
    
    public int getVertexCount(){
    	return vertexCount;
    }
 
}