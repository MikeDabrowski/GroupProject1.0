package moje;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import grpr.Graph;
import piotrek.Sciezka;

public class DaneTestowe {

	//private static ArrayList<Sciezka> daneTestowe = new ArrayList<>();
	//generuje pliki startowe
	public static void wielkiTest1(int dataSize) throws FileNotFoundException{
		File plik = new File("src/moje/piotrekTest.txt");
		File plik2 = new File("src/moje/dijkstraTest.txt");
		try {
			Scanner odczyt = new Scanner(new File("src/moje/piotrekTest.txt"));
			Scanner odczyt2 = new Scanner(new File("src/moje/dijkstraTest.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter out = new PrintWriter("src/moje/piotrekTest.txt");
		PrintWriter out2 = new PrintWriter("src/moje/dijkstraTest.txt");
		
		//zmienne
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		//losowanie
		for(int i=0;i<dataSize;i++){
			first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			dist=ThreadLocalRandom.current().nextInt(1,100);
			velo=ThreadLocalRandom.current().nextInt(1,10);
			int time=(int)dist/velo;
			
			//System.out.println(first+" "+second+" "+dist+" "+velo+" "+time);
			out.println(first+" "+second+" "+dist+" "+velo);
			out2.println(first+" "+second+" "+time);
		}
		
		out.close();
		out2.close();
	}
	public static File przygotujPlikTestowy(int dataSize) throws FileNotFoundException{
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH-mm-ss-SSS");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		File file=new File("src/moje/plikTestowy_"+dataSize+"_"+strDate);
		PrintWriter output = new PrintWriter("src/moje/plikTestowy_"+dataSize+"_"+strDate+".txt");
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		if(dataSize<650){
			for(int i=0;i<dataSize;i++){
				first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
				second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
				dist=ThreadLocalRandom.current().nextInt(1,100);
				velo=ThreadLocalRandom.current().nextInt(1,10);
							
				//System.out.println(first+" "+second+" "+dist+" "+velo+" "+time);
				output.println(first+" "+second+" "+dist+" "+velo);
			}
			output.close();
		}
		else if(dataSize>=650 && dataSize <240000){
			
		}
		return file;
	}
	public static File przygotujPlikTestowy2(int dataSize) throws FileNotFoundException{
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH-mm-ss-SSS");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		File file=new File("src/moje/plikTestowy_"+dataSize+"_"+strDate+".txt");
		PrintWriter output = new PrintWriter("src/moje/plikTestowy_"+dataSize+"_"+strDate+".txt");
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		if(dataSize<650){
			for(int i=0;i<dataSize;i++){
				first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
				second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
				dist=ThreadLocalRandom.current().nextInt(1,100);
				velo=ThreadLocalRandom.current().nextInt(1,10);
							
				System.out.println(first+"\t"+second+"\t"+dist+"\t"+velo);
				output.println(first+" "+second+" "+dist+" "+velo);
			}
			output.close();
		}
		else if(dataSize>=650 && dataSize <240000){
			for(int i=0;i<dataSize;i++){//zrob tyle punktow
				//losuj jaki typ nazwy punktu a czy aa
				int typ=ThreadLocalRandom.current().nextInt(1, 702);
				int typ2=ThreadLocalRandom.current().nextInt(1, 702);
				if(typ<=26 && typ2<=26){//nazwa a-a
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
					second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
				}
				else if(typ<=26 && typ2>26){//a-aa
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
					second=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
				}
				else if(typ>26 && typ2<=26){//aa-a
					second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
				}
				else{//aa-aa
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
					second=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
				}
				dist=ThreadLocalRandom.current().nextInt(1,100);
				velo=ThreadLocalRandom.current().nextInt(1,10);
							
				System.out.println(first+"\t"+second+"\t"+dist+"\t"+velo);
				output.println(first+" "+second+" "+dist+" "+velo);
			}
			output.close();
		}
		return file;
	}	
	public static File przygotujPlikTestowy3(String start, String end, int points, int edges) throws FileNotFoundException{

		//start and end is to be sure those points are included
		ArrayList<String> P=new ArrayList<>();//will store points
		int number=new File("src/moje/").list().length;
		number++;
		
		File file=new File("src/moje/plikTestowy_"+number+"_P"+points+"_E"+edges+"_"+start+"_"+end+".txt");
		PrintWriter output = new PrintWriter("src/moje/plikTestowy_"+number+"_P"+points+"_E"+edges+"_"+start+"_"+end+".txt");
		System.out.println("File plikTestowy_"+number+"= new File(\"src/moje/"+file.getName()+"\");");
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		
		//dodaj start i end
		P.add(start);
		P.add(end);
		
		//losuj points punkt�w
		int typ;
			while(P.size()<points) {
				typ=ThreadLocalRandom.current().nextInt(0, 3);
				if(typ==0){//determines a or aa point
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
					if(!P.contains(first)){
						P.add(first);
					}
				}else if(typ==1){
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
					if(!P.contains(first)){
						P.add(first);
					}
				}else{
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
					if(!P.contains(first)){
						P.add(first);
					}
				}
			}
			System.out.println(P.size());
			ArrayList<String> FP=new ArrayList<>();//will store points
			ArrayList<String> SP=new ArrayList<>();//will store points
			
			//losuj edges sciezek
			int i=0;
			while (i<edges) {
				first=P.get(ThreadLocalRandom.current().nextInt(0, P.size()));
				FP.add(first);
				second=P.get(ThreadLocalRandom.current().nextInt(0, P.size()));
				SP.add(second);
				dist=ThreadLocalRandom.current().nextInt(1,100);
				velo=ThreadLocalRandom.current().nextInt(1,10);
				output.println(first+" "+second+" "+dist+" "+velo);
				i++;
			}
		output.close();
		 
		//check if file has path
		return file;
		
	}
	public static File przygotujPlikTestowy3R(String start, String end, int points, int edges) throws FileNotFoundException{
		//start and and is to be sure those points are included
		ArrayList<String> P=new ArrayList<>();//will store points
		ArrayList<String> E=new ArrayList<>();//will store edges
		
		File file=new File("src/moje/plikTestowy_P"+points+"_E"+edges+".txt");
		PrintWriter output = new PrintWriter("src/moje/plikTestowy_P"+points+"_E"+edges+".txt");
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		
		//dodaj start i end
		P.add(start);
		P.add(end);
		
		//losuj points punkt�w
			while(P.size()<points) {
				if(ThreadLocalRandom.current().nextBoolean()){//determines a or aa point
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
					if(!P.contains(first)){
						P.add(first);
					}
				}else{
					first=znaki[ThreadLocalRandom.current().nextInt(0,25)]+znaki[ThreadLocalRandom.current().nextInt(0,25)];
					if(!P.contains(first)){
						P.add(first);
					}
				}
			}
		//losuj edges sciezek
		int i=0;
		while (i<edges) {
			first=P.get(ThreadLocalRandom.current().nextInt(0, P.size()));
			second=P.get(ThreadLocalRandom.current().nextInt(0, P.size()));
			dist=ThreadLocalRandom.current().nextInt(1,100);
			velo=ThreadLocalRandom.current().nextInt(1,10);
			output.println(first+" "+second+" "+dist+" "+velo);
			i++;
		}
		output.close();
		return file;
	}
	public static ArrayList<Sciezka> przygotujTest2(ArrayList<Sciezka> daneTestowe,int dataSize){
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		for(int i=0;i<dataSize;i++){
			first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			dist=ThreadLocalRandom.current().nextInt(1,100);
			velo=ThreadLocalRandom.current().nextInt(1,10);
			
			daneTestowe.add(new Sciezka(first,second,dist,velo));
		}
		
		return daneTestowe;
	}
	public static  Graph.Edge[] przygotujTestD1(int dataSize,boolean showData){
		Graph.Edge[] Graph2=new Graph.Edge[dataSize];
		String[] znaki={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","w","v","x","z"};
		String first, second;
		int dist, velo;
		
		for(int i=0;i<dataSize;i++){
			first=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			second=znaki[ThreadLocalRandom.current().nextInt(0,25)];
			dist=ThreadLocalRandom.current().nextInt(1,100);
			velo=ThreadLocalRandom.current().nextInt(1,10);
			int time=(int)dist/velo;
			if(showData==true)
				System.out.println(first+", "+second+", "+time);
			Graph2[i]=new Graph.Edge(first,second, time);
		}
		return Graph2;
	}
	public static  Graph.Edge[] przygotujTestD2(File fileName, boolean showData) throws FileNotFoundException{
		
		Scanner in2 = new Scanner(fileName);
		int lines=0;
		while(in2.hasNext()){
			lines++;
			in2.nextLine();
		}
		in2.close();
		Scanner in = new Scanner(fileName);
		
		Graph.Edge[] Graph2=new Graph.Edge[lines];
		
		String temp1,temp2;
		int temp3,temp4;
		double temp5;
		int i=0;
		while(in.hasNext()){
	    	temp1=in.next();
	    	temp2=in.next();
	    	temp3=in.nextInt();
    		temp4=in.nextInt();
	    	temp5=(double)temp3/(double)temp4;
	    	if(showData==true)
	    		System.out.println(temp1+" "+temp2+" "+temp3+" "+temp4+" "+temp5);
	    	Graph2[i]=new Graph.Edge(temp1,temp2,temp5);
	    	i++;
	    }
		in.close();
		return Graph2;		
	}
	/*public static void pokazDane(ArrayList<Sciezka> daneTestowe){
		for(int i=0;i<daneTestowe.size();i++){
			System.out.println(i+" ("+daneTestowe.get(i).getPktPoczatkowy()+", "+
								   daneTestowe.get(i).getPktKoncowy()+", "+
								   daneTestowe.get(i).getDlugosc()+", "+
								   daneTestowe.get(i).getPredkosc()+")");
		}
	}*/
	public static void pokazDaneD1(Graph.Edge[] Graph2){
		for(int i=0;i<Graph2.length;i++){
			System.out.println("("+Graph2.toString());
		}
	}
	public static void pokazPlik(File file) throws FileNotFoundException{
		Scanner input=new Scanner(file);
		while(input.hasNext()){
			System.out.println(input.nextLine());
		}
	}
	private class Edge{
		String first;
		String second;
		int dist;
		int velo;
		public Edge(String first1,String second1,int dist1,int velo1){
			this.first=first1;
			this.dist=dist1;
			this.second=second1;
			this.velo=velo1;					
		}
	}
}
