package moje;

import moje.Graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import moje.Edge;
import moje.Vertex;
/*
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphModel;
import org.jgrapht.*;
import org.jgrapht.ext.*;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.ListenableDirectedGraph;
import org.jgrapht.graph.*;*/

public class Main extends JApplet {

	private static final Color     DEFAULT_BG_COLOR = Color.decode( "#FAFBFF" );
	private static final Dimension DEFAULT_SIZE = new Dimension( 530, 320 );
//	private static  JGraphModelAdapter m_jgAdapter;
	
	public static void main(String[] args){
		// TODO Auto-generated method stub
		/*
		LinkedList<Vertex> vertexList = new LinkedList<Vertex>();
		Graph graf=new Graph(10);
		graf.addVertex();
		graf.addVertex();
		graf.addVertex();
		graf.addVertex();
		graf.addEdge(0, 1, 5);
		graf.addEdge(0, 2, 5);
		graf.addEdge(1, 2, 5);
		graf.addEdge(2, 3, 5);
		graf.addEdge(3, 1, 5);
		graf.addEdge(2, 0, 5);
		vertexList=graf.getVertexList();
		System.out.println(graf.getVertexCount());
		System.out.println(graf.getVertexList());

		
		// Construct Model and Graph
        GraphModel model = new DefaultGraphModel();
        SimpleWeightedGraph<String, DefaultWeightedEdge>  graph2 = 
        		new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class); 
        JGraph graph = new JGraph(model);
        // Control-drag should clone selection
        graph.setCloneable(true);

        // Enable edit without final RETURN keystroke
        graph.setInvokesStopCellEditing(true);

        // When over a cell, jump to its default port (we only have one, anyway)
        graph.setJumpToDefaultPort(true);

        // Insert all three cells in one call, so we need an array to store them
        DefaultGraphCell[] cells = new DefaultGraphCell[5];
        // Create Hello Vertex
        cells[0] = createVertex("cell0", 20, 20, 40, 20, null, false );

        // Create World Vertex
        cells[1] = createVertex("cell1", 140, 140, 40, 20,Color.ORANGE, true);

        cells[2] = createVertex("cell2", 80, 20, 40, 20, null, false );
        cells[3] = createVertex("cell3", 100, 100, 100, 200, null, false);
        cells[4] = createVertex("cell4", 120, 100, 100, 200, null, false);
        // Create Edge
        DefaultEdge edge = new DefaultEdge("foo");
        DefaultEdge edge2 = new DefaultEdge("foo2");
        // Fetch the ports from the new vertices, and connect them with the edge
        edge.setSource(cells[1].getChildAt(0));
        edge.setTarget(cells[0].getChildAt(0));
        
        cells[2] = edge;//set cell as line/arrow/edge
        edge2.setSource(cells[1].getChildAt(0));
        edge2.setSource(cells[1].getChildAt(0));
        cells[3] = edge2;

        // Create Edge
        DefaultEdge edge1 = new DefaultEdge();
        // Fetch the ports from the new vertices, and connect them with the edge
//        cells[0].addPort();
//        cells[1].addPort();
//        edge1.setSource(cells[1]);
//        edge1.setTarget(cells[0]);
//        cells[3] = edge1;

        // Set Arrow Style for edge
        int arrow = GraphConstants.ARROW_CLASSIC;
        GraphConstants.setLineEnd(edge.getAttributes(), arrow);
        GraphConstants.setEndFill(edge.getAttributes(), true);

        // Insert the cells via the cache, so they get selected
        graph.getGraphLayoutCache().insert(cells);
                
        // Show in Frame
        JFrame frame = new JFrame();
        frame.getContentPane().add(new JScrollPane(graph));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        */
        
        
		/*
		// Construct Model and Graph
        GraphModel model = new DefaultGraphModel();
        SimpleWeightedGraph<String, DefaultWeightedEdge>  graph2 = 
        		new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class); 
        JGraph graph = new JGraph(model);
        // Control-drag should clone selection
        graph.setCloneable(true);

        // Enable edit without final RETURN keystroke
        graph.setInvokesStopCellEditing(true);

        // When over a cell, jump to its default port (we only have one, anyway)
        graph.setJumpToDefaultPort(true);

        // Insert all three cells in one call, so we need an array to store them
        DefaultGraphCell[] cells = new DefaultGraphCell[5];
        // Create Hello Vertex
        cells[0] = createVertex("cell0", 20, 20, 40, 20, null, false );

        // Create World Vertex
        cells[1] = createVertex("cell1", 140, 140, 40, 20,Color.ORANGE, true);

        cells[2] = createVertex("cell2", 80, 20, 40, 20, null, false );
        cells[3] = createVertex("cell3", 100, 100, 100, 200, null, false);
        cells[4] = createVertex("cell4", 120, 100, 100, 200, null, false);
        // Create Edge
        DefaultEdge edge = new DefaultEdge("foo");
        DefaultEdge edge2 = new DefaultEdge("foo2");
        // Fetch the ports from the new vertices, and connect them with the edge
        edge.setSource(cells[1].getChildAt(0));
        edge.setTarget(cells[0].getChildAt(0));
        
        cells[2] = edge;//set cell as line/arrow/edge
        edge2.setSource(cells[1].getChildAt(0));
        edge2.setSource(cells[1].getChildAt(0));
        cells[3] = edge2;

        // Create Edge
        DefaultEdge edge1 = new DefaultEdge();
        // Fetch the ports from the new vertices, and connect them with the edge
//        cells[0].addPort();
//        cells[1].addPort();
//        edge1.setSource(cells[1]);
//        edge1.setTarget(cells[0]);
//        cells[3] = edge1;

        // Set Arrow Style for edge
        int arrow = GraphConstants.ARROW_CLASSIC;
        GraphConstants.setLineEnd(edge.getAttributes(), arrow);
        GraphConstants.setEndFill(edge.getAttributes(), true);

        // Insert the cells via the cache, so they get selected
        graph.getGraphLayoutCache().insert(cells);
                
        // Show in Frame
        JFrame frame = new JFrame();
        frame.getContentPane().add(new JScrollPane(graph));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);*/
	
	}
	/*
	  public static DefaultGraphCell createVertex(String name, double x,
	            double y, double w, double h, Color bg, boolean raised) {

	        // Create vertex with the given name
	        DefaultGraphCell cell = new DefaultGraphCell(name);
	        
	        // Set bounds
	        GraphConstants.setBounds(cell.getAttributes(),
	                new Rectangle2D.Double(x, y, w, h));

	        // Set fill color
	        if (bg != null) {
	            GraphConstants.setGradientColor(cell.getAttributes(), bg);
	            GraphConstants.setOpaque(cell.getAttributes(), true);
	        }

	        // Set raised border
	        if (raised) {
	            GraphConstants.setBorder(cell.getAttributes(),
	                    BorderFactory.createRaisedBevelBorder());
	        } else // Set black border
	        {
	            GraphConstants.setBorderColor(cell.getAttributes(),
	                    Color.black);
	        }
	        // Add a Floating Port
	        cell.addPort();

	        return cell;
	    }
*/

}
